# Bulk Mailer

Bulk mailer is a serial mail sender with time delay and random integer adder to content and title.

The aim of the application is sending bulk mails without using *cc* and *bcc* and prevent sender mail adress being marked as spam while doing this.

Just add {i} to anywhere in title or/and content.


***


**Example:**

**Config =>**

	Wait Time : 30

**Title =>**

	System Message ({i})

**Content =>**

	This is a system message for some reason. msgnum: {i}


***


sends mail to every mail adress in list with waiting 30 seconds between message send (depends on mail hosting requirements). replaces {i} with a random number like below

**Title =>**

	System Message (854726)

**Content =>**

	This is a system message for some reason. msgnum: 854726