# -*- coding: utf-8 -*-
import json
import os
import random
import smtplib
import sys
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import pyqtSignal, QThread, QTranslator
from PyQt5.QtWidgets import QMainWindow, QApplication

from bulkmailer_design import Ui_MainWindow

__author__ = 'ozgur'
__creation_date__ = '11.01.2019' '21:45'

# Map translate function
_translate = QtCore.QCoreApplication.translate

# Check Bundle dir path
if getattr(sys, 'frozen', False):
    # we are running in a bundle
    # noinspection PyUnresolvedReferences
    BUNDLE_DIR = sys._MEIPASS
else:
    # we are running in a normal Python environment
    BUNDLE_DIR = os.path.dirname(os.path.abspath(__file__))

TPATH = os.path.join(BUNDLE_DIR, "i18n")


class Config:
    def __init__(self, initdict: dict = None):
        if not initdict:
            initdict = {}
        self.port = initdict.get("port", 587)
        self.email = initdict.get("email", "")
        self.smtp = initdict.get("smtp", "")
        self.langcode = initdict.get("langcode", "en")
        self.waittime = initdict.get("sleeptime", 30)
        self.tls = initdict.get("tls", False)

    @staticmethod
    def _get_file_path() -> str:
        """
        returns the path concat of home folder and bulkmailer.conf\n
        :return:
        """
        return os.path.join(str(Path.home()), "bulkmailer.conf")

    def load(self):
        """
        loads this object from file\n
        :return:
        :exception: raises exceptions except FileNotFoundError
        """
        try:
            with open(self._get_file_path(), "r+") as ofile:
                sdict = json.loads(ofile.read(), encoding="UTF-8")
                if isinstance(sdict, dict):
                    self.__dict__.update(sdict)

        except FileNotFoundError:
            pass
        except Exception as ex:
            raise ex

    def save(self):
        """
        saves this object to file \n
        :return:
        """
        with open(self._get_file_path(), "w+") as ofile:
            ofile.write(json.dumps(self.__dict__, ensure_ascii=False, indent=4))


class Worker(QThread):
    workingon = pyqtSignal(str)
    mailsent = pyqtSignal(str)
    mailcantsent = pyqtSignal(str)
    finished = pyqtSignal()
    usermessage = pyqtSignal(str)

    def __init__(self, parent, config: Config, password: str):

        super().__init__(parent)
        self.maillist = []
        self.title = ""
        self.content = ""
        self.config = config
        self.password = password

    def run(self):

        if not self.title or not self.content \
                or not self.maillist or not isinstance(self.maillist, list):
            return
        fromaddr = self.config.email
        passwd = self.password
        sleeptime = 0
        for toaddress in self.maillist:

            time.sleep(sleeptime)
            sleeptime = self.config.waittime
            try:
                server = smtplib.SMTP(self.config.smtp, self.config.port)
                if self.config.tls:
                    server.starttls()
                server.login(fromaddr, passwd)
                self.workingon.emit(str(toaddress))

                msgnum = str(random.randint(100000, 999999))

                msg = MIMEMultipart()
                msg['From'] = fromaddr
                msg['To'] = toaddress
                msg['Subject'] = self.title.replace("{i}", msgnum)
                body = self.content.replace("{i}", msgnum)
                msg.attach(MIMEText(body, 'plain'))
                text = msg.as_string()
                server.sendmail(fromaddr, toaddress, text)
                self.mailsent.emit(toaddress)

                server.quit()
            except Exception as ex:
                # noinspection PyArgumentList
                self.mailcantsent.emit("{} : {}").format(toaddress, str(ex))

        self.finished.emit()


class MainForm(QMainWindow, Ui_MainWindow):
    def __init__(self, qapp: QApplication):
        super(self.__class__, self).__init__()
        self.setupUi(self)

        self.jobcount = 0
        self.jobtotal = 0
        self.qapp = qapp

        # check config
        self.config = Config()
        self._load_config()
        self._translate(str(self.cmb_lang.currentText()))

        #set worker and bind slots
        self.worker = Worker(self, self.config, "")
        self.worker.workingon.connect(self.slot_workingon)
        self.worker.mailsent.connect(self.slot_mailsent)
        self.worker.mailcantsent.connect(self.slot_mailcantsent)
        self.worker.finished.connect(self.slot_finished)
        self.worker.usermessage.connect(self.slot_usermessage)

        # set icon
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(os.path.join(BUNDLE_DIR, "assets", "mail.ico")),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)

    def _translate(self, langcode: str):
        """
        Changes message and gui translations with given language code\n
        :param langcode: tr, en etc.
        :return: None
        """
        # linguist
        print(TPATH)
        translator = QTranslator()
        translator.load(os.path.join(TPATH, "bulkmailer_{}.qm".format(langcode)))
        self.qapp.installTranslator(translator)
        self.retranslateUi(self)

    # noinspection PyArgumentList
    def _fields_have_error(self) -> False or str:
        """
        Validates fields and returns error message if has else returns False\n
        :return: False or error message
        """

        if not self.txt_title.text().strip():
            retval = _translate("msg", "You have to insert a title!")
        elif not self.txt_content.toPlainText().strip():
            retval = _translate("msg", "You have to insert content!")
        elif not self.txt_targetlist.toPlainText().strip():
            retval = _translate("msg", "You have to insert email list!")
        else:
            retval = False
        return retval

    def _load_config(self):
        """
        Loads config and sets fields
        :return:
        """

        # noinspection PyBroadException
        try:
            self.config.load()
        except Exception:
            # noinspection PyArgumentList
            self.lbl_info.setText(_translate("msg", "Couldn't load configuration"))

        conf = self.config
        self.num_port.setValue(conf.port)
        self.txt_emailfrom.setText(conf.email)
        self.txt_smtp.setText(conf.smtp)
        index = self.cmb_lang.findText(conf.langcode, QtCore.Qt.MatchFixedString)
        if index >= 0:
            self.cmb_lang.setCurrentIndex(index)
        self.num_wait.setValue(conf.waittime)
        self.chk_tls.setChecked(conf.tls)

    def _set_config(self):
        """
        Sets config object's values from gui\n
        :return:
        """
        self.config.port = int(self.num_port.value())
        self.config.email = self.txt_emailfrom.text()
        self.config.smtp = self.txt_smtp.text()
        self.config.langcode = str(self.cmb_lang.currentText())
        self.config.waittime = int(self.num_wait.value())
        self.config.tls = self.chk_tls.isChecked()

    def event_btn_save_clicked(self):
        self._set_config()
        # noinspection PyBroadException
        try:
            self.config.save()
        except Exception:
            # noinspection PyArgumentList
            self.lbl_info.setText(_translate("msg", "Couldn't save configuration"))

    def event_cmb_lang_textchanged(self, text: str):
        self._translate(text)

    def event_btn_send_clicked(self):
        """
        Triggered when mail send button clicked \n
        :return: None
        """
        if not self.worker.isRunning():
            fielderror = self._fields_have_error()
            if fielderror:
                self.lbl_info.setText(fielderror)
                return
            self._set_config()
            self.worker.config = self.config
            self.worker.password = str(self.txt_password.text())

            self.worker.title = self.txt_title.text()
            self.worker.content = self.txt_content.toPlainText()
            rawlist = self.txt_targetlist.toPlainText().split("\n")
            mlist = []
            for mail in rawlist:
                if "@" in mail.strip():
                    mlist.append(mail.strip())
            self.worker.maillist = mlist.copy()

            self.jobtotal = len(self.worker.maillist)
            self.prg_status.setMaximum(self.jobtotal)
            self.txt_sent.setPlainText("")
            self.txt_notsent.setPlainText("")
            self.worker.start()
        else:
            # noinspection PyArgumentList
            self.lbl_info.setText(_translate("msg", "Still sending..."))

    def slot_workingon(self, email: str):
        """
        Triggered when mail sending started\n
        :param email: email address
        :return:
        """
        self.jobcount += 1
        self.prg_status.setValue(self.jobcount)
        # noinspection PyArgumentList
        self.lbl_info.setText(
            _translate("Sending : {0} ... {1}/{2}").format(email, self.jobcount, self.jobtotal))

    def slot_mailsent(self, email: str):
        """
        Triggered when mail sent\n
        :param email: email address
        :return:
        """
        self.txt_sent.appendPlainText(email)

    def slot_mailcantsent(self, email: str):
        """
        Triggered when an exception occur while mail sending\n
        :param email: email address
        :return:
        """
        self.txt_notsent.appendPlainText(email)

    def slot_finished(self):
        """
        Triggered when send job finished\n
        :return:
        """
        print("finished")
        self.jobcount = 0
        self.jobtotal = 0
        self.prg_status.setMaximum(1)
        self.prg_status.setValue(0)
        # noinspection PyArgumentList
        self.lbl_info.setText(_translate("msg", "Completed"))

    def slot_usermessage(self, msg: str):
        """
        prints message in lbl_info\n
        :param msg: message to show
        :return:
        """
        self.lbl_info.setText(msg)

        print(msg)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    form = MainForm(app)
    form.show()
    app.exec_()
    sys.exit()
