# -*- mode: python -*-

block_cipher = None


a = Analysis(['bulkmailer.py'],
             pathex=['/home/ozgur/Desktop/myworks/workspaces/pyCharmProjects/bulkmailer'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          Tree('i18n', prefix='i18n'),
          Tree('assets', prefix='assets'),
          name='bulkmailer.bin',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , icon='assets/mail.ico')
