<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="tr_TR" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../bulkmailer_design.py" line="187"/>
        <source>Bulk Mailer</source>
        <translation>Bulk Mailer</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="188"/>
        <source>Content:</source>
        <translation>İçerik:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="189"/>
        <source>Target:</source>
        <translation>Hedef:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="190"/>
        <source>Title:</source>
        <translation>Başlık:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="180"/>
        <source>Mail content here...</source>
        <translation type="obsolete">e-posta içeriği buraya...</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="193"/>
        <source>email adress per line...</source>
        <translation>her satıra bir eposta adresi...</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="194"/>
        <source>Send</source>
        <translation>Gönder</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="195"/>
        <source>Sent:</source>
        <translation>Gönderilmiş:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="196"/>
        <source>Fail:</source>
        <translation>Gönderilememiş:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="197"/>
        <source>Sender</source>
        <translation>Gönderici</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="209"/>
        <source>Config</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="204"/>
        <source>SMTP Address : </source>
        <translation>SMTP Adresi : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="147"/>
        <source>SMTP port : </source>
        <translation type="obsolete">SMTP portu</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="198"/>
        <source>mysecretpassword</source>
        <translation>benimgizliparolam</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="155"/>
        <source>en</source>
        <translation type="obsolete">en</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="156"/>
        <source>tr</source>
        <translation type="obsolete">tr</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="205"/>
        <source>Password : </source>
        <translation>Parola : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="201"/>
        <source>smtp.mail.com</source>
        <translation>smtp.mail.com</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="160"/>
        <source>587</source>
        <translation type="obsolete">587</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="202"/>
        <source>E-Mail : </source>
        <translation>E-Posta : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="200"/>
        <source>test@mail.com</source>
        <translation>test@mail.com</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="208"/>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="199"/>
        <source>SMTP Port : </source>
        <translation>SMTP Port : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="203"/>
        <source>Language : </source>
        <translation>Dil : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="194"/>
        <source>Wait Time(seconds):</source>
        <translation type="obsolete">Bekleme Zamanı(saniye):</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="207"/>
        <source>Secure Connection(TLS):</source>
        <translation>Güvenli Bağlantı(TLS):</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="206"/>
        <source>Wait Time(s):</source>
        <translation>Bekleme Zamanı(sn):</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="191"/>
        <source>You can use {i} in here, example: System Message ({i})</source>
        <translation>Burada {i} kullanabilirsiniz,  örnek: Sistem Mesajı ({i})</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="192"/>
        <source>Mail content here. You can also use {i} in here to prevent your mail adress marked as spam. For example System Message({i}) produces =&gt; System Message (175239)</source>
        <translation>E-posta içeriği buraya gelecek. E-posta adresinizin spam olarak işaretlenmesini engellemek için burada da {i} kullanabilirsiniz. Örneğin Sistem Mesajı({i})  şu şekilde gönderilir=&gt; Sistem Mesajı(175239)</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="210"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Bulk Mailer&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 1.0&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Bulk Mailer&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 1.0&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="216"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
</context>
<context>
    <name>msg</name>
    <message>
        <location filename="../bulkmailer.py" line="312"/>
        <source>Completed</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="185"/>
        <source>You have to insert a title!</source>
        <translation>Başlık girmelisiniz!</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="187"/>
        <source>You have to insert content!</source>
        <translation>İçerik girmelisiniz!</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="189"/>
        <source>You have to insert email list!</source>
        <translation>E-posta listesi girmelisiniz!</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="271"/>
        <source>Still sending...</source>
        <translation>Hala gönderiliyor...</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="205"/>
        <source>Couldn&apos;t load configuration</source>
        <translation>Ayarlar yüklenemedi</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="236"/>
        <source>Couldn&apos;t save configuration</source>
        <translation>Ayarlar kaydedilemedi</translation>
    </message>
</context>
</TS>
