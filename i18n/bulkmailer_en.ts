<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../bulkmailer_design.py" line="187"/>
        <source>Bulk Mailer</source>
        <translation>Bulk Mailer</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="188"/>
        <source>Content:</source>
        <translation>Content:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="189"/>
        <source>Target:</source>
        <translation>Target:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="190"/>
        <source>Title:</source>
        <translation>Title:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="180"/>
        <source>Mail content here...</source>
        <translation type="obsolete">Mail content here...</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="193"/>
        <source>email adress per line...</source>
        <translation>email adress per line...</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="194"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="195"/>
        <source>Sent:</source>
        <translation>Sent:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="196"/>
        <source>Fail:</source>
        <translation>Fail:</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="197"/>
        <source>Sender</source>
        <translation>Sender</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="204"/>
        <source>SMTP Address : </source>
        <translation>SMTP Address : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="198"/>
        <source>mysecretpassword</source>
        <translation>mysecretpassword</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="205"/>
        <source>Password : </source>
        <translation>Password : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="201"/>
        <source>smtp.mail.com</source>
        <translation>smtp.mail.com</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="160"/>
        <source>587</source>
        <translation type="obsolete">587</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="202"/>
        <source>E-Mail : </source>
        <translation>E-Mail : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="200"/>
        <source>test@mail.com</source>
        <translation>test@mail.com</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="208"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="209"/>
        <source>Config</source>
        <translation>Config</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="199"/>
        <source>SMTP Port : </source>
        <translation>SMTP Port : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="203"/>
        <source>Language : </source>
        <translation>Language : </translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="194"/>
        <source>Wait Time(seconds):</source>
        <translation type="obsolete">Wait Time(seconds):</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="207"/>
        <source>Secure Connection(TLS):</source>
        <translation>Secure Connection(TLS):</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="206"/>
        <source>Wait Time(s):</source>
        <translation>Wait Time(s):</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="191"/>
        <source>You can use {i} in here, example: System Message ({i})</source>
        <translation>You can use {i} in here, example: System Message ({i})</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="192"/>
        <source>Mail content here. You can also use {i} in here to prevent your mail adress marked as spam. For example System Message({i}) produces =&gt; System Message (175239)</source>
        <translation>Mail content here. You can also use {i} in here to prevent your mail adress marked as spam. For example System Message({i}) produces =&gt; System Message (175239)</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="210"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Bulk Mailer&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 1.0&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Bulk Mailer&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 1.0&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bulkmailer_design.py" line="216"/>
        <source>About</source>
        <translation>About</translation>
    </message>
</context>
<context>
    <name>msg</name>
    <message>
        <location filename="../bulkmailer.py" line="312"/>
        <source>Completed</source>
        <translation>Completed</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="185"/>
        <source>You have to insert a title!</source>
        <translation>You have to insert a title!</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="187"/>
        <source>You have to insert content!</source>
        <translation>You have to insert content!</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="189"/>
        <source>You have to insert email list!</source>
        <translation>You have to insert email list!</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="271"/>
        <source>Still sending...</source>
        <translation>Still sending...</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="205"/>
        <source>Couldn&apos;t load configuration</source>
        <translation>Couldn&apos;t load configuration</translation>
    </message>
    <message>
        <location filename="../bulkmailer.py" line="236"/>
        <source>Couldn&apos;t save configuration</source>
        <translation>Couldn&apos;t save configuration</translation>
    </message>
</context>
</TS>
